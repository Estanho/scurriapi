from setuptools import setup, find_packages, Extension

setup(
    name="ScurriApi",
    version='1.0.0',
    url='https://bitbucket.org/Estanho/scurriapi/overview',
    maintainer='Guilherme Caminha',
    maintainer_email='gpkc@cin.ufpe.br',
    setup_requires=['pytest-runner'],
    tests_require=['pytest', 'pytest-cov'],
    install_requires=[],
    packages=find_packages(),
    license='LICENSE'
)
