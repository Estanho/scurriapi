from typing import Union


def three_five(n) -> Union[int, str]:
    """ Generates all the numbers from 1 to n+1.
    :param n: int
    :return:
        If the current number is divisible by 3, returns the string 'Three'.
        If the current number is divisible by 5, returns the string 'Five'.
        If the current number is divisible by 3 and 5, returns the string 'ThreeFive'.
        Otherwise, returns the number as an integer.

    """

    for i in range(1, n + 1):

        if i % 3 == 0 and i % 5 == 0:
            yield 'ThreeFive'

        elif i % 3 == 0:
            yield 'Three'

        elif i % 5 == 0:
            yield 'Five'

        else:
            yield i

