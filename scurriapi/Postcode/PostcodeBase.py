from functools import wraps


class PostcodeBase:

    @property
    def is_valid(self) -> bool:
        raise NotImplementedError

    @staticmethod
    def _postcode_must_be_valid(f):
        @wraps(f)
        def wrapped(inst, *args, **kwargs):
            if inst.is_valid:
                return f(inst, *args, **kwargs)
            raise ValueError("The provided postcode must be valid!")
        return wrapped
