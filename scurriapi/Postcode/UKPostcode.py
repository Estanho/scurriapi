import re

from .PostcodeBase import PostcodeBase


class UKPostcode(PostcodeBase):

    # Extracted from https://en.wikipedia.org/wiki/Postcodes_in_the_United_Kingdom#Formatting
    PATTERN = "^([Gg][Ii][Rr] 0[Aa]{2})|((([A-Za-z][0-9]{1,2})|(([A-Za-z][A-Ha-hJ-Yj-y][0-9]{1,2})|(([A-Za-z][0-9][" \
              "A-Za-z])|([A-Za-z][A-Ha-hJ-Yj-y][0-9]?[A-Za-z])))) [0-9][A-Za-z]{2})$"

    def __init__(self, postcode: str) -> None:
        """ Represents a UK postcode. All properties are calculated lazily, so
        the postcode property can be changed if necessary.
        All properties except `is_valid` will raise a ValueError if the postcode is not valid.
        :param postcode:
            String containing the formatted postcode.
        """
        self.postcode = postcode

    @property
    def is_valid(self) -> bool:
        """ Verifies if the postcode is valid.
        :return:
            True if the postcode is valid, or False otherwise.
        """
        if re.match(UKPostcode.PATTERN, self.postcode):
            return True
        else:
            return False

    @property
    @PostcodeBase._postcode_must_be_valid
    def outward_code(self) -> str:
        """ The outward code is the part of the postcode before the single space in the middle.
        """
        return self.postcode.split()[0]

    @property
    @PostcodeBase._postcode_must_be_valid
    def inward_code(self) -> str:
        """ The inward code is the part of the postcode after the single space in the middle.
        """
        return self.postcode.split()[1]

    @property
    @PostcodeBase._postcode_must_be_valid
    def postcode_area(self) -> str:
        """ The postcode area is made up by all characters before the first number in the inward code.
        """
        return re.match("([a-zA-Z]*)", self.postcode).groups()[0]

    @property
    @PostcodeBase._postcode_must_be_valid
    def postcode_district(self) -> str:
        """ The postcode district is made of the final one or two digits in the outward code
        and sometimes a final letter.
        """
        return re.search("([0-9]*[a-zA-Z])? ", self.postcode).groups()[0]

    @property
    @PostcodeBase._postcode_must_be_valid
    def postcode_sector(self) -> str:
        """ The postcode sector is made of the first character in the inward code.
        """
        return self.inward_code[0]

    @property
    @PostcodeBase._postcode_must_be_valid
    def postcode_unit(self) -> str:
        """ The postcode unit is two characters added to the end of the postcode sector.
        """
        return self.inward_code[1:]
