## Scurri API Challenge

This challenge is presented in three parts:

 * Telling about a difficult technical problem I had to solve;
 * Solving a problem of multiples;
 * Creating a library API to validate UK post codes.


### Requirements

This software is tested under Python versions `3.6.4` and `3.5.5` (see `bitbucket-pipelines.yml`).
I'm using [PEP484 Type Annotations](https://www.python.org/dev/peps/pep-0484/)
so the minimum version should be `3.5`.

For the tests, [pytest](https://docs.pytest.org/en/latest/) is used. You can
install all test / development requirements with:

`$ pip install pytest pytest-runner pytest-cov`


### Building and Installing

This challenge is implemented as a Python package. To build and install it,
please run on your terminal:

`python setup.py install`


### Running Tests

For this challenge, several unit testes were created.
To run those, you can execute the following command on your terminal:

`python setup.py test`

Coverage is automatically generated when running this command. A detailed
html version can be found on `cov_html/intex.html`.


### Technical Problem

A technical problem I've faced that I'm proud of solving was when I was an
undergrad student and participated in a national competition against many
other universities. I needed to use some image processing algorithms to detect
the position of a given object in a camera. However, for that to be feasible,
I needed to find a way to tune the platform I was building my project on so that
I would be able to achieve at least 30 processed frames per second, making the
project feel considerably real time. That was something no other team was ever
able to do with the platform in several years. After a lot of research and
hacking, I finally managed to do it. This made the project possible and I was
awarded the First Prize. This victory made it possible for me to compete in the
international version of the competition in China, where I was awarded the Second
Prize against several dozens of universities around the world.


### Problem of Multiples

The problem of multiples is implemented on the `scurriapi.ThreeFive` package.

To run it, you can execute the following command on your terminal:

`python -m scurriapi.ThreeFive`

This will fire up the module file `scurriapi/ThreeFive/__main__.py`, which
then runs `scurriapi.ThreeFive.three_five` appropriately.

The solution uses a generator function, yielding the appropriate result for
the current number being processed.


### UK Postcodes

#### Description and Solution

The problem of validating UK postcodes and providing a library API for the
validator is implemented on the `scurriapi.Postcode` package.

The library is presented as a data structure for storing postcodes. A base
class (`scurriapi.Postcode.PostcodeBase`) is used to provide common functionality
that would be used by other postcode representations, such as a US postcode
representation.

All classes that inherit from `scurriapi.Postcode.PostcodeBase` must
implement the `is_valid` property. A decorator named `_postcode_must_be_valid`
is also provided and can be used to wrap methods or properties that require the
postcode to be valid. When used, if the postcode is invalid, those methods or properties
will raise a `ValueError` exception.

The `scurriapi.Postcode.UKPostcode` implements the basic functionality for a
UK postcode representation. It inherits from `scurriapi.Postcode.PostcodeBase`
and therefore exposes the `is_valid` property. Its constructor receives as argument
the postcode to be represented. Other exposed properties from the API are:

 * `outward_code`
 * `inward_code`
 * `postcode_area`
 * `postcode_district`
 * `postcode_sector`
 * `postcode_unit`
 
All these properties are wrapped with the `_postcode_must_be_valid` decorator,
and therefore, may raise a `ValueError` exception if the internal postcode is not valid.

For the validating functionality, the library uses the regular expression provided
on the [Wikipedia page](https://en.wikipedia.org/wiki/Postcodes_in_the_United_Kingdom#Formatting). 


#### Usage

Several usage examples are provided within the `tests/test_postcode.py` file. It uses
fixtures for test parametrization, which can be found at `tests/conftest.py`.

Below is one usage example:


``` python

>>> from scurriapi.Postcode import UKPostcode
>>> postal_code = 'EC1A 1BB'
>>> uk_postcode = UKPostcode(postal_code)
>>> uk_postcode.is_valid
True
>>> uk_postcode.inward_code
'1BB'
>>> uk_postcode.postcode_area
'EC'
```