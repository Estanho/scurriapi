import pytest


VALID_POSTCODES = [
    'EC1A 1BB',
    'W1A 0AX',
    'M1 1AE',
    'B33 8TH',
    'CR2 6XH',
    'DN55 1PT',
    'AA9A 9AA',
    'A9A 9AA',
    'A9 9AA',
    'A99 9AA',
    'AA9 9AA',
    'AA99 9AA'
]

INVALID_POSTCODES = [
    'AAA',
    'AAAA AAA',
    'AA AAA',
    '999 999',
    '99A 9AA',
    '9A9 9AA',
    '999 AA9',
    '99/;345'
]


@pytest.fixture(params=VALID_POSTCODES)
def valid_postcode(request):
    return request.param


@pytest.fixture(params=INVALID_POSTCODES)
def invalid_postcode(request):
    return request.param