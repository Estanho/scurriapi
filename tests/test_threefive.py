from scurriapi.ThreeFive import three_five


class TestThreeFive:

    def test_empty(self):
        gen = three_five(0)

        assert len(list(gen)) == 0

    def test_single_item(self):
        gen = three_five(1)
        items = list(gen)

        assert items[0] == 1
        assert len(items) == 1

    def test_strings(self):
        gen = three_five(30)
        items = list(gen)

        assert items[2] == 'Three'
        assert items[4] == 'Five'
        assert items[29] == 'ThreeFive'
        assert len(items) == 30
