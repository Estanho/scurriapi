import pytest

from scurriapi.Postcode import PostcodeBase, UKPostcode


class TestPostcodeBase:

    def test_is_valid_raise_NotImplementedError(self):
        postcode = PostcodeBase()

        with pytest.raises(NotImplementedError):
            postcode.is_valid

    def test_postcode_must_be_valid(self):
        class DummyPostcode(PostcodeBase):
            @property
            def is_valid(self):
                return True

            @PostcodeBase._postcode_must_be_valid
            def get_100(self):
                return 100

        dummy_postcode = DummyPostcode()

        assert dummy_postcode.get_100() == 100

    def test_postcode_must_be_valid_raises_ValueError(self):
        class DummyPostcode(PostcodeBase):
            @property
            def is_valid(self):
                return False

            @PostcodeBase._postcode_must_be_valid
            def should_raise(self):
                pass

        dummy_postcode = DummyPostcode()

        with pytest.raises(ValueError):
            dummy_postcode.should_raise()


class TestUKPostCode:

    def test_valid_postcode(self, valid_postcode):
        uk_postcode = UKPostcode(valid_postcode)

        assert uk_postcode.is_valid

    def test_invalid_postcode(self, invalid_postcode):
        uk_postcode = UKPostcode(invalid_postcode)

        assert not uk_postcode.is_valid

    def test_outward_code(self):
        uk_postcode = UKPostcode("AA99 9AA")

        assert uk_postcode.outward_code == "AA99"

    def test_inward_code(self):
        uk_postcode = UKPostcode("AA99 9AA")

        assert uk_postcode.inward_code == "9AA"

    def test_postcode_area(self):
        uk_postcode = UKPostcode("EC1A 1BB")

        assert uk_postcode.postcode_area == "EC"

    def test_postcode_district(self):
        uk_postcode = UKPostcode("EC1A 1BB")

        assert uk_postcode.postcode_district == "1A"

    def test_postcode_sector(self):
        uk_postcode = UKPostcode("EC1A 1BB")

        assert uk_postcode.postcode_sector == "1"

    def test_postcode_unit(self):
        uk_postcode = UKPostcode("EC1A 1BB")

        assert uk_postcode.postcode_unit == "BB"